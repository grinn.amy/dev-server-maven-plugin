package info.tygr;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;

@Mojo(name = "start", defaultPhase = LifecyclePhase.NONE)
public class DevServerPlugin extends AbstractMojo {

    public void execute() throws MojoExecutionException, MojoFailureException {
        /* noop */
    }
}
